#!/usr/bin/env python
import logging
import os
import re
import json
from dotenv import load_dotenv

from api import package_create, package_delete, package_search, package_show, package_update
from common import check_to_update, delete_missing

logging.basicConfig(filename='./log/harvester.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO)

# This is unique to single env; Need to change
to_replace = {
    "owner_org": "5bb66c6c-5549-41a1-82fc-d9b5c04670f8" ,
    "org_title_at_publication":"agriculture_and_agrifood_canada" 
    }

LLI = 'lli'
REG = 'reg'
     
def load_json(file):
    data =  None
    with open(file) as json_fp:
        data = json.load(json_fp)
    return data

def get_location_key(raw_location):
    def location_lookup(raw_location):
        location = ''
        loc = raw_location.lower()
        if 'quebec' in loc:
            location = "livinglabquebec"
        elif 'eastern p' in loc:
            location = "livinglabeasternp"
        elif 'atlantic' in loc:
            location = "livinglabatl"
        elif 'ontar' in loc:
            location = "livinglabontario"
        elif 'british co' in loc:
            location = "livinglabbritishcolumbia"
        return location
    location_key = ''
    match = re.match("(Living.?Labs?)", raw_location, re.IGNORECASE)
    # if living labs in kew
    if match:       
        location_key = location_lookup(raw_location)
    return location_key

def process_keywords(data, check_against):
    '''
    make keywords compatible to lli
    :param data:
    :param check_against:
    :return:
    '''
    check_against_ll_kws = check_against[0]
    check_against_ll_theme = check_against[2]
    kw_replacing = []
    location = []
    theme =[]
    
    if "keywords" in data.keys():
        kw_english = data["keywords"]["en"]
        for k in kw_english:
            k2 = k[0].upper() + k[1:]
            if k2 in check_against_ll_kws:
                kw_replacing.append(check_against_ll_kws[k2])
        for k in kw_english: # Location
            true_location = get_location_key(k)
            if true_location:
                location.append(true_location)
        for k in kw_english: # Theme
            k2 = k[0].upper() + k[1:]
            if k2 in check_against_ll_theme:
                theme.append(check_against_ll_theme[k2])
    
    if (len(kw_replacing) == 0):
        logging.debug("Missing keywords! Setting keyword to 'unknown' for '{}'".format(data['title_translated']['en']))
        logging.debug('Keywords: {}'.format(kw_english))
        kw_replacing.append("unknown")
    if (len(theme) == 0):
        logging.debug("Missing theme! Setting theme to 'general' for '{}'".format(data['title_translated']['en']))
        logging.debug('Keywords: {}'.format(kw_english))
        theme.append(u'general-val')
    
    if (len(location) == 0):   
        logging.debug("missing location! Not Harvesting: {}".format(data['title_translated']['en']))
        logging.debug('Keywords: {}'.format(kw_english))
        return None

    data["keywords"] = kw_replacing
    data[u'theme'] = theme
    data[u'location'] = location
    return data

def temp_process(data):
    if "frequency" in data.keys():
        if data["frequency"] in ("not_planned","unknown"):
            data["frequency"] = "as_needed"

    if len(data['resources']) > 0:
        for r in data['resources']:
            if "language" in r.keys():
                for l in r["language"]:
                    if l == "bilingual":
                        idx = r["language"].index("bilingual")
                        r["language"][idx] = "other"
            else:
                logging.info("no language in resource")

def transform_data(data, keys_to_remove,items_to_add, kw_dict, items_to_replace = None):
    '''
    Transform the incoming data by remove some items and add some items with default value to match the
    target schema

    :param data:
    :param keys_to_remove:
    :param items_to_add:
    :return:
    '''
    # Set org
    owner_org_id = os.getenv("organization_id", None)
    if owner_org_id != None:
        items_to_replace['owner_org'] = owner_org_id

    transformed = []
    for d in data:
        # remove items
        for key in keys_to_remove:
            d.pop(key,"default")
        for k,v in items_to_add.items():
            d[k] = v
        for k, v in items_to_replace.items():
            d[k] = v
        # remove "extra" fields for some old Registry datasets
        d.pop("extras","default")

        result = process_keywords(d, kw_dict)

        #other temperary conversion to get around
        if result:
            # XXX What does this even do?
            temp_process(d)
            transformed.append(d)

    return transformed

def get_all_reg_data():
    # XXX filter datasets first so that you don't hit 1k limit
    '''
    retrieve all data from the remote site
    :return:
    '''
    search_query = {
        'rows': '1000',}
    result = package_search(REG, search_query )
    
    new_result = {"results": []}

    for i in result['results']:
        if 'extras' in i:
            # If dataset was published by LLI, don't harvest it
             if any('value' in item and item['key'] == 'lli-published' for item in i['extras']):
                 continue
        if "keywords" not in i:
            # No keywords, this check seems to be for so that we don't through exception
            continue
        kwes=[]

        kws = i["keywords"]["en"]
        remove = True
        for kw in kws:
            kwes.append(kw)
            match = re.match("(Living.?Labs?)", kw, re.IGNORECASE)
            if match:
                remove = False
        if not remove:
            new_result['results'].append(i)
    result = new_result
    return result

def extract_n_transform():
    all_data = get_all_reg_data()

    keys_to_remove = load_json("Data/keysToRemove.json")
    items_to_add = load_json("Data/fieldsAddedForLli.json")
    rev_dict = load_json("Data/reverse_kw_dict.json")

    extracted = all_data["results"]

    return transform_data(
        extracted, keys_to_remove, 
        items_to_add, rev_dict, to_replace
    )

def harvest_registry():
    '''
    Harvest data from Registry and transform to LLI format
    :param from_og: if True, harvest from OG, otherwise harvest from Registry
    :param id: if not None, harvest only the dataset with the given id
    :param extra_params: a dictionary of extra parameters
    '''
    dest = os.getenv("destination_url")
    source = os.getenv("source_url")

    if dest == None or len(dest) == 0 or dest.find("http") == -1:
        logging.info("variable 'site' is missing")
        return

    logging.info("Harvesting data from {} to destination {}".format(source, dest))
    
    try:
        search_query = {'rows': '1000'}
        resp = package_search(LLI, search_query)
        lli_pkg_id_list = [item["id"] for item in resp["results"]]
    except Exception as ex:
        logging.error("Unable to query LLI for existing datasets")
        logging.error(ex)
        print('cannot connect to LLI')
        exit()

    dataset_list = extract_n_transform()

    confirmed_ids = []
    dataset_name = []
    harvest_label = {
        'key': 'lli-harvested',
        'value': True
    }

    for d in dataset_list:
        confirmed_ids.append(d['id'])

        d['extras'] = [harvest_label]
        
        if d['id'] in lli_pkg_id_list:
            # Existing Package
            update = check_to_update(d, LLI)
            if update:
                try:
                    resp = package_update(LLI, data_dict=d)
                    logging.info("UPDATE Found - updating dataset %s with ID: %s" % (d['title_translated']['en'], d['id'] ))
                    dataset_name.append('UPDATED %s ID %s' % (d['title_translated']['en'], d['id'] ))
                except Exception as ec:
                    logging.error(ec)
                    logging.error("Failed to UPDATE package %s with ID: %s" % (d['title_translated']['en'], d['id'] ))             
        else:
            # New package to create   
            try: 
                resp = package_create(LLI, data_dict=d)
                logging.info("CREATE New dataset Found - creating dataset %s with ID: %s" % (d['title_translated']['en'], d['id'] ))
                dataset_name.append('CREATED %s ID %s' % (d['title_translated']['en'], d['id'] ))
            except Exception as ev:
                logging.error(ev)
                logging.error("Failed to CREATE package %s with ID: %s" % (d['title_translated']['en'], d['id'] ))
                try:
                    resp = package_update(LLI, data_dict=d)
                    logging.info("Undeleting pacakge on REG  %s with ID: %s" % (d['title_translated']['en'], d['id'] ))
                except Exception as ev:
                    logging.error(ev)
                    logging.error("Failed to UN-DELETE package %s with ID: %s" % (d['title_translated']['en'], d['id'] ))
              
    delete_missing(confirmed_ids, 'lli-harvested', LLI)

if __name__ == "__main__":
    load_dotenv()
    harvest_registry()