#!/usr/bin/env python
# -*- coding: cp1250 -*-
import json
import os
from dotenv import load_dotenv
from datetime import datetime
import logging

from api import package_create, package_delete, package_search, package_show, package_update
from common import check_to_update, delete_missing

logging.basicConfig(filename='./log/publisher.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO)

LLI = 'lli'
REG = 'reg'
# following fields were left over of old version. They are for registry only
to_remove = ["aafc_sector", "procured_data", "procured_data_organization_name", "authoritative_source",
             "drf_program_inventory", "data_steward_email", "elegible_for_release", "publication",
             "data_source_repository", "aafc_note", "drf_core_responsibilities",
             "mint_a_doi", "other", "ineligibility_reason", "authority_to_release", "privacy", "formats", "security",
             "official_language", "access_to_information", "organization", "groups"
             ]
to_replace = {"type": "dataset", "owner_org": "45826094-5c88-4ae8-803f-a684012cfdb2",
              "collection": "primary", "jurisdiction": "federal",
              "procured_data_organization_name":"N/A"
              }
to_convert = [
    "keywords", "org_title_at_publication", "odi_reference_number","resources"
    ]
to_enforce_mandatory = {
    "creator":"creator@agc.gc.ca", "resources":{"date_published":"2000-01-01"}
    }

confirmed_ids = []

def load_json(file):
    data =  None
    with open(file) as json_fp:
        data = json.load(json_fp)
    return data

def convert_kw(kw):
    kw_lli = {}
    with open("Data/keywords_as.json", "r") as fi: #reverse the keywords from lli format to normal format
        kw_lli = json.load(fi)
    converted = {"fr": [], "en": []}
    for i in kw:
        if i in kw_lli: #put en and fr instead of key for text
            converted["en"].append(kw_lli[i][0])
            converted["fr"].append(kw_lli[i][1])
        else:
            converted["en"].append(i)
            converted["fr"].append(i)
    return converted

def convert_orgt(orgt):
    converted = {}
    converted["en"] = orgt
    converted["fr"] = orgt
    return converted

def convert_odi(original = None):
    return "ODI-2018-2222"

def convert_resource(resources):
    for res in resources:
        for k, v in res.items():
            if k == 'language':
                new_v = []
                for lang in v:
                    if lang == 'bilingual':
                        new_v.append('other')
                    else:
                        new_v.append(lang)
                res[k] = new_v
            if k == 'date_published' and v == '':
                res[k] = str(datetime.today())

    return resources

conversion = {"keywords": convert_kw, "org_title_at_publication": convert_orgt,
                  "odi_reference_number": convert_odi,"resources":convert_resource}


def get_n_post(package_id, reg_ids):

    lli_package = package_show(LLI, package_id)
    return push_a_package(lli_package, package_id, reg_ids)      


def push_a_package(og_data, package_id, reg_ids, lli=True ):

    for k, v in to_replace.items():
        if lli and k == "type":
            continue
        og_data[k] = to_replace[k]

    # Turn Theme items into keywords for Reg
    for theme in og_data['theme']:
        og_data['keywords'].append(theme)
    for location in og_data['location']:
        og_data['keywords'].append(location)
    
    lli_kw_to_drop = ['unknown', 'general']
    for kw in lli_kw_to_drop:
        if kw in og_data['keywords']: og_data['keywords'].remove(kw)
        
    if lli:  # Only convert under lli
        for k in to_convert:
            if k in og_data.keys():
                og_data[k] = conversion[k](og_data[k])
            else:
                og_data[k] = conversion[k]()
    to_remove_lli = load_json("Data/fieldsAddedForLli.json").keys()
    if lli:  # When the case is lli as oppose to registry
        to_remove_final = to_remove_lli
    else:
        to_remove_final = to_remove  # keep original version which was developed for Registry
    # remove
    for k in to_remove_final:
        del og_data[k]
    # check mandatory, if missing value then add the default value
    # Only in lli
    for k, v in to_enforce_mandatory.items():
        if k in og_data.keys():
            if isinstance(v, dict):
                for kk, vv in v.items():
                    for res  in og_data[k]:
                        if kk in res.keys():
                            if res[kk] == "":
                                og_data[k][kk] = vv 
                        else:
                            res[kk] = vv

            else:
                if og_data[k] == "":
                    og_data[k] = v
        else:
            og_data[k] = v

    og_data['__type'] = 'internal'
    og_data['owner_org'] = 'aafc-7c-aac'
    
    # Remote location and keyword fields
    del og_data['theme']
    del og_data['location']
    harvest_label = {
        'key': 'lli-published',
        'value': True
    }
    og_data['extras'] = [harvest_label]
    confirmed_ids.append(package_id)
 
    if package_id in reg_ids:
        update = check_to_update(og_data, REG)
        if update:
            try:
                resp = package_update(REG, data_dict=og_data)
                logging.info("UPDATE Found - updating dataset %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
            except Exception as ec:
                    logging.error(ec)
                    logging.error("Failed to UPDATE package %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
    else:
        # New package to create     
        try: 
            resp = package_create(REG, data_dict=og_data)
            logging.info("CREATE New dataset Found - creating dataset %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
        except Exception as ev:
            logging.error(ev)
            logging.error("Failed to CREATE package %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
            try:
                # Case when package was deleted on REG but we are updating it on LLI; prevents issue where package is not found
                # because state=deleted, but we can't update it, because the ID is still in use
                resp = package_update(REG, data_dict=og_data)
                logging.info("Undeleting pacakge on REG  %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
            except Exception as ev:
                logging.error(ev)
                logging.error("Failed to UN-DELETE package %s with ID: %s" % (og_data['title_translated']['en'], og_data['id'] ))
                
    
    return True


def publish():
    """
    Push a single dataset from LLI to Registry
    :return:
    """
    dest = os.getenv("destination_url")
    source = os.getenv("source_url")
    logging.info("Publishing data from {} to destination {}".format(source, dest))
    # get packages from LLI to publish
    
    # resp = package_show(REG, id='09297ff5-6c71-4b8a-841f-218cc38a525e')
    # print(resp)
    # exit()
    search_query = {
        'rows': '1000',
        'fq': 'publication:open_government' }
    resp = package_search(LLI, search_query )
    lli_pkg_id_list = [item["id"] for item in resp["results"]]
    lli_pkg_list = resp["results"]
    harvested_pkgs = []
    
    # get packages from Reg to check for updates
    # XXX possibly check if dataset is in certain org?
    search_query = {
        'rows': '1000' }
    resp = package_search(REG, search_query )
    reg_pkg_id_list = [item["id"] for item in resp["results"]]
    
    
    for pkg in lli_pkg_list:
        if 'extras' in pkg:
            if any('value' in item and item['key'] == 'lli-harvested' for item in pkg['extras']):
                harvested_pkgs.append(pkg['id'])
            
            
    packages_to_publish = [pkg for pkg in lli_pkg_id_list if pkg not in harvested_pkgs]
    
    print(lli_pkg_id_list)
    print(harvested_pkgs)
    print(packages_to_publish)
    for id in packages_to_publish:
        res = get_n_post(id, reg_pkg_id_list)
    
    # Removed delete on publish because it goes against TBS policy see #AR-553
    # delete_missing(confirmed_ids, 'lli-published', REG)
      
if __name__ == "__main__":
    load_dotenv()
    publish()
