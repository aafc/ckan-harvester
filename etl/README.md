# What gets picked up?

### The harvester pulls data from REG to LLI, it ensures the following when pulling datasets:
1. Dataset keyword matching regex `"(Living.?Labs?)"` must exist in the `keywords[en]` field
2. Dataset must not contains the extra keyword `lli-published` (indicating that it was published from LLI originally)
3. Dataset is public

Additionally, it performs the following:
1. If no Keywords exists. Adds a new Keyword 'Unknown'
2. If no Keyword matching an LLI Theme exists. Add a new Theme 'General'


### The publisher pushed data from LLI to REG, it ensure the following when pushing datasets:
1. Publication Source is AFFC
2. Must not contains the extra keyword `lli-harvested` (indicating that it was harvested from REG originally)
3. Dataset is public


# How to use Harvester
Run `harvest_reg.py` to harvest datasets from the Data Reg project to the LLI project.

Run `publish_livinglabs.py` to publish datasets from the LLI project to the REG project. 



# Note about harvest script


## Required
You will need to populate the .env environment with API keys from both harvesting
```
<!-- REG -->
source_url=
source_api_key

<!-- LLI -->
destination_url=
destination_api_key=

organization_id=
```

## Usage and notes:
In real production, a CRON job is scheduled to run the harvest and publish process every 10 minutes









