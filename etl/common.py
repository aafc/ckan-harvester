#!/usr/bin/env python
import logging

from api import package_create, package_delete, package_search, package_show, package_update

def check_to_update(d, site):
    """_summary_

    Args:
        d (dict): Reg Dataset
        lli_ckan lli_ckan (ckanapi.RemoteCKAN): An instance of the CKAN API client connected to the CKAN instance.

    Returns:
        bool: True or False if dataset had been updated since last pull
    """
    try:
        remote_pkg = package_show(site, id=d['id'])
    except Exception as ec:
        logging.error("Failure getting package by ID on LLI, CKANAPI error")
        logging.info(ec)
    
    remote_date = remote_pkg['metadata_modified']
    source_date = d['metadata_modified']
   
    if source_date > remote_date:
        print(d['title_translated']['en'])
        print("source date: {} - remote date: {}".format(source_date, remote_date))
        return True
    else:
        return False
    
def delete_missing(confirmed_ids, key, site):
    """
    Delete CKAN packages with a specific 'extras' field ('harvested') that are not in the list of confirmed IDs.

    Args:
        confirmed_ids (list): A list of package IDs that are confirmed and should not be deleted.
        lli_ckan (ckanapi.RemoteCKAN): An instance of the CKAN API client connected to the CKAN instance.

    Returns:
        None

    This function searches for CKAN packages with the 'harvested' extras field and checks if their IDs are in the list
    of confirmed IDs. If a package is found in the search results but not in the list of confirmed IDs, it is deleted.
    Any exceptions (e.g., 'NotFound' or 'ValidationError') are caught and appropriate messages are printed.

    Example:
    delete_missing(['confirmed-id-1', 'confirmed-id-2'], lli_ckan)
    """

    search_query = {
        'q': f'extras_{key}:*',
        'rows': '1000'}
    
    search_results = package_search(site, search_query)
    package_ids = [result['id'] for result in search_results['results']]
    
    # Create a new list of IDs that are in the first list but not in the second list
    datasets_to_remove = [_id for _id in package_ids if _id not in confirmed_ids]

    for package_id in datasets_to_remove:
        try:
            package_delete(site, id=package_id)
            logging.info(f"Deleted package with ID: {package_id}")
        except Exception as e:
            logging.error(f"Package with ID {package_id} not found.")
            