#!/usr/bin/env python
import logging
import os
import re
import json
from datetime import datetime, timedelta
import urllib
import requests
from urllib3.exceptions import InsecureRequestWarning
import urllib3
from random import randint
from dotenv import load_dotenv

# disable SSL issues from displaying XXX temp fix
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

LLI = 'lli'
REG = 'reg'

def call_api(site, endpoint, method='get', _json=None, query=None):
    if site == 'lli':
        url = os.getenv("destination_url")
        api = os.getenv("destination_api_key")
        
    elif site == 'reg':
        url = os.getenv("source_url")
        api = os.getenv("source_api_key")
    else:
        print('no site provided')
        exit()
        
    query_str = str()
    if query:
        query_str = list()
        for key, value in query.items():
            query_str.append(f'{ key }={ "%20".join(value.split(" ")) }')
        query_str = '&'.join(query_str)
    
    headers = dict()
    body = None
    if _json:
        body = json.dumps(_json)
        headers['Content-Type'] = 'application/json'
        
    headers['Authorization'] = api
    if site == REG:
        full_url = f'{ url }/api/lli/action/{ endpoint }?{ query_str }'
    
    else:
        full_url = f'{ url }/api/3/action/{ endpoint }?{ query_str }'
    resp = requests.request(
        method, full_url,
        data=body,
        headers=headers,
        verify=False
    )
        
    if resp.status_code != 200:
        logging.error(f'{ full_url } returned { resp.status_code }: { resp.text }')

    result = resp.json()
    if not result['success']:
        logging.error(f'{ full_url } return non-success: { resp.text }')
    return result

def package_create(site, data_dict: dict) -> dict:
    return call_api(
        site,
        'package_create',
        method='post',
        _json=data_dict
        )['result']
    
def package_update(site, data_dict: dict) -> dict:
    return call_api(
        site,
        'package_patch',
        method='post',
        _json=data_dict
        )['result']

def package_delete(site, id: str) -> dict:
    return call_api(
        site,
        'package_delete',
        method='post',
        _json={ 'id': id },
        )['result']
    
def package_show(site, id: str) -> dict:
    return call_api(
        site,
        'package_show',
        query={'id': id }
        )['result']
    
def package_search(site, query):
    return call_api(
        site,
        'package_search',
        query=query
        )['result']
    